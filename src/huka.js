//     huka.js 1.0
//     sweet, sweet nodeLists with a jQuery-like API

;(function (c, d, h) {

	// __$__( `selector` )
	//
	// Main function, also aliased as __window.huka__( `selector` )
	//
	// Returns a wrapped querySelectorAll nodeList with custom prototype

	this.$ = this.huka = function (a) {
		return new $[d].i(a)
	};

	//
	// huka.prototype
	// ---------------
	//
	h = {

		//
		// Base setup
		// ----------
		//
		
		// Default collection length
		length: 0,

		// Magically return an array like representation instead of an object
		splice: c.splice,

		//
		// Core methods
		// ------------
		//

		// __i__( `selector` )
		//
		// Internal init method called by the main $ function. Converts a nodeList to an array-like object.
		i: function (a) {
			c.push.apply(this, a && a.nodeType ? [a] : "" + a === a ? c.slice.call(document.querySelectorAll(a)) : null)
		},

		// __each__( `function` )
		//
		// apply the function to each item in the collection
		each: function (a, b) {
			c.forEach.call(b = this, a)
			return b
		},

		// __extend__( `object` )
		//
		// shallow merge an object literal into the huka prototype
		extend: function(a, b) {
			for(b in a) $[d][b] = a[b]
		}

	};

	// Set prototypes
	$.fn = $[d] = h.i[d] = h

}([], 'prototype'))