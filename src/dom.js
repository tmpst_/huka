//		dom.js 1.0
//		huka plugin that provides dom manipulation functionality
//

;(function($, c, p, i){
	
	//
	// Setup
	// -------
	//
	
	
	// Convert a string to a dom node and prepend or append to each item in a collection
	i = function(a, b) {
		a = ("" + a === a) ? (p.innerHTML = a) && p.firstChild : a
		return this.each(function(e){
			e.parentNode.insertBefore(a.cloneNode(true), b ? e.nextSibling : e)
		})
	}
	
	// Reusable element used to convert a string to an html fragment
	p = document.createElement('div')
	
	
	// 
	// Extend huka.prototype
	// ---------------------
	//
	return $.fn.extend({


	//
	// Attribute methods
	// -----------------
	//
	

	// __setAttribute__( `property, value || { key: 'value' }` )
	//
	// sets an attribute on each item in the collection
	setAttribute: function(a,b) {
		return this.each(function(e){
			if("" + a === a) e.setAttribute(a, b || '')
			else for(var c in a) e.setAttribute(c, a[c] || '')
		})
	},
	
	// __removeAttribute__( `property || { key: 'any value' }` )
	//
	// removes an attribute from each item in a collection
	removeAttribute: function(a,b) {
		return this.each(function(e){
			if("" + a === a) e.removeAttribute(a)
			else for(var c in a) e.removeAttribute(c)  
		})
	},
	
	// __getAttribute__( `property` )
	//
	// get the value of an attribute from the first item in a collection
	getAttribute: function(a) {
		return this[0].getAttribute(a);
	},
	
	//
	// Visibility methods
	// ------------------
	//
	
	// __hide__()
	//
	// hide all items in a collection
	hide: function() {
		return this.setAttribute(c)
	},
	
	// __show__()
	//
	// show all items in a collection
	show: function(){
		return this.removeAttribute(c)
	},
	
	// __toggle__()
	//
	// toggles the hidden attribute for each item in a collection
	toggle: function(a) {
		
		return this.each(function(e){
			a = (e.getAttribute(c) !== null ? 'remove' : 'set') + 'Attribute'
			e[a](c) 
		})
	},
	
	//
	// Dom manipulation methods
	// ------------------------
	//

	// __prepend__( `html string || dom node` )
	//
	// prepends an html string or dom node to each item in a collection
	prepend: function(a) {
		return i.call(this, a)
	},
	
	// __append__( `html string || dom node` )
	//
	// appends an html string or dom node to each item in a collection
	append: function(a) {
		return i.call(this, a, true)
	},
	
	// __remove__()
	//
	// removes dom nodes matching the selector
	remove: function() {
		return this.each(function(e){
			e.parentNode.removeChild(e)
		})
	},

	// __replacewith__( `html string || dom node` )
	//
	// replaces an existing dom node with an html string or dom node
	replacewith: function(c){
		return this.prepend(c).remove()
    }




})

}(huka, 'hidden'))