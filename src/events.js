//		eventst.js 1.0
//		huka plugin that provides event handling functionality
//

;(function($, c) {

	//
	// Extend huka.prototype
	// ---------------------
	//
	return $.fn.extend({

		// __on__( `event, function` )
		//
		// Register a callback function for an event on each item in a collection
		on: function(a, b) {
			return this.each(function(e) {
				e['add' + c](a, b)
			})
		},


		// __off__( `event, function` )
		//
		// Deregister a callback function for an event on each item in a collection
		off: function(a, b) {
			return this.each(function(e){
				e['remove' + c](a, b)
			})
		},

		// __trigger__( `event` )
		//
		// Fire an event on each item in a collection
		trigger: function(a, b) {
			return this.each(function(e){
				b  = new Event(a, this, true)
				e.dispatchEvent(b)
			})
		}
	})

})(huka, 'EventListener')