;(function($){

	var request = function(method, url, callback, data) {
		method = method.toUpperCase()
		var xhr = new XMLHttpRequest();
		xhr.open(method, url, true);
		if (method == 'POST') {
			xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
			xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest')
		}	
		xhr.onreadystatechange = function () {
			if (xhr.readyState === 4) {
				if (callback) callback(xhr.responseText, xhr.status);
				return status;
			}
		}
		xhr.send(data || '');
	}
	
	$.fn.extend({

		post: function(url, callback, data) {
			return request('POST', url, callback, data)
		},
	
		get: function(url, callback, data) {
			request('GET', url, callback, data)
		},
		
		ajax: function(method, url, callback, data) {
			request(method, url, callback, data)
		}
	})



})(huka)