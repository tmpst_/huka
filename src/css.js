//     css.js 1.0
//     huka plugin that provides css and classList functionality
//

;(function ($, c, s, f, i, u) {

	//
	// Setup
	// -----
	//


	// Process the collection using native classList API
	i = function (a, b, d) {
		return (f.test(typeof a)) ?
			d.each(function (e, i) {
				e[c][b](a.call(e, i))
			}):
			a.split(s).forEach(function (i) {
				d.each(function (e) {
					e[c][b](i)
				})
			}), d
	}
	
	// Unitless css properties
	u = {
		'column-count': 1,
		'columns': 1,
		'font-weight': 1,
		'line-height': 1,
		'opacity': 1,
		'z-index': 1,
		'zoom': 1
	}

	
	//
	// Extend huka.prototype
	// ---------------------
	//
	
	return $.fn.extend({
	
		//
		// CSS getter/setter
		// -----------------
		//
		// Implements __jQuery.css__  which acts as a getter or
		// setting depending on the params that are passed into the function

		// __css__( `property [ , value ] || { key: 'value' }` )
		//
		css: function (a, b) {
			var 
				c = '',
				d = "" + a === a,
				e = d && arguments.length < 2 && this[0],
				f = function (a, b) {
					if (b === false) this.each(function (e) {
							e.style.removeProperty(a)
						})
					else c += ';' + a + ':' + ((isFinite(b) && !u[a]) ? b + 'px' : b)
				}
			
			// Return the value of the css property from the first item in the collection
			if (e) {
				b = a.replace(/-+(.)?/g, function (a, b) { return b ? b.toUpperCase() : ''})
				return e.style[b] || getComputedStyle(e).getPropertyValue(a)
			}
			// Collect the css rules to apply or immediately remove properties where `value === false`
			if (d) f.call(this, a, b)
			else for (b in a) f.call(this, b, a[b])


			// Return the collection applying CSS rules if required
			return c ? this.each(function (e) {
				e.style.cssText += c
			}) : this
		},

		//
		// ClassList API methods
		// ---------------------
		//

		// __addClass__( `value [value] || function` )  
		// set the className(s) on each item in a collection 
		addClass: function (a) {
			return i(a, 'add', this)
		},

		// __removeClass__( `value [value] || function` )  
		// remove the className(s) on each item in a collection
		removeClass: function (a) {
			return i(a, 'remove', this)
		},
	
		// __toggleClass__( `value [value] || function` )  
		// toggle the className(s) on each item in a collection
		toggleClass: function (a) {
			return i(a, 'toggle', this)
		},
	
		// __hasClass__( `value [value]` )  
		// returns boolean `true` if every item in the collection has the className(s)
		hasClass: function (a, b, i, d) {
			i = this.length
			b = "" + a === a
			var foo = function (a) { return d[c].contains(a) }
			while (b && i--) {
				d = this[i];
				b = a.split(s).every(foo)
			}
			return b
		}
	})

}(huka, 'classList', /\s+/, /^f/))

