//     huka.js 1.0
//     sweet, sweet nodeLists with a jQuery-like API

;(function (c, d, h) {

	// __$__( `selector` )
	//
	// Main function, also aliased as __window.huka__( `selector` )
	//
	// Returns a wrapped querySelectorAll nodeList with custom prototype

	this.$ = this.huka = function (a) {
		return new $[d].i(a)
	};

	//
	// huka.prototype
	// ---------------
	//
	h = {

		//
		// Base setup
		// ----------
		//
		
		// Default collection length
		length: 0,

		// Magically return an array like representation instead of an object
		splice: c.splice,

		//
		// Core methods
		// ------------
		//

		// __i__( `selector` )
		//
		// Internal init method called by the main $ function. Converts a nodeList to an array-like object.
		i: function (a) {
			c.push.apply(this, a && a.nodeType ? [a] : "" + a === a ? c.slice.call(document.querySelectorAll(a)) : null)
		},

		// __each__( `function` )
		//
		// apply the function to each item in the collection
		each: function (a, b) {
			c.forEach.call(b = this, a)
			return b
		},

		// __extend__( `object` )
		//
		// shallow merge an object literal into the huka prototype
		extend: function(a, b) {
			for(b in a) $[d][b] = a[b]
		}

	};

	// Set prototypes
	$.fn = $[d] = h.i[d] = h

}([], 'prototype'))
;(function($){

	var request = function(method, url, callback, data) {
		method = method.toUpperCase()
		var xhr = new XMLHttpRequest();
		xhr.open(method, url, true);
		if (method == 'POST') {
			xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
			xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest')
		}	
		xhr.onreadystatechange = function () {
			if (xhr.readyState === 4) {
				if (callback) callback(xhr.responseText, xhr.status);
				return status;
			}
		}
		xhr.send(data || '');
	}
	
	$.fn.extend({

		post: function(url, callback, data) {
			return request('POST', url, callback, data)
		},
	
		get: function(url, callback, data) {
			request('GET', url, callback, data)
		},
		
		ajax: function(method, url, callback, data) {
			request(method, url, callback, data)
		}
	})



})(huka)
//     css.js 1.0
//     huka plugin that provides css and classList functionality
//

;(function ($, c, s, f, i, u) {

	//
	// Setup
	// -----
	//


	// Process the collection using native classList API
	i = function (a, b, d) {
		return (f.test(typeof a)) ?
			d.each(function (e, i) {
				e[c][b](a.call(e, i))
			}):
			a.split(s).forEach(function (i) {
				d.each(function (e) {
					e[c][b](i)
				})
			}), d
	}
	
	// Unitless css properties
	u = {
		'column-count': 1,
		'columns': 1,
		'font-weight': 1,
		'line-height': 1,
		'opacity': 1,
		'z-index': 1,
		'zoom': 1
	}

	
	//
	// Extend huka.prototype
	// ---------------------
	//
	
	return $.fn.extend({
	
		//
		// CSS getter/setter
		// -----------------
		//
		// Implements __jQuery.css__  which acts as a getter or
		// setting depending on the params that are passed into the function

		// __css__( `property [ , value ] || { key: 'value' }` )
		//
		css: function (a, b) {
			var 
				c = '',
				d = "" + a === a,
				e = d && arguments.length < 2 && this[0],
				f = function (a, b) {
					if (b === false) this.each(function (e) {
							e.style.removeProperty(a)
						})
					else c += ';' + a + ':' + ((isFinite(b) && !u[a]) ? b + 'px' : b)
				}
			
			// Return the value of the css property from the first item in the collection
			if (e) {
				b = a.replace(/-+(.)?/g, function (a, b) { return b ? b.toUpperCase() : ''})
				return e.style[b] || getComputedStyle(e).getPropertyValue(a)
			}
			// Collect the css rules to apply or immediately remove properties where `value === false`
			if (d) f.call(this, a, b)
			else for (b in a) f.call(this, b, a[b])


			// Return the collection applying CSS rules if required
			return c ? this.each(function (e) {
				e.style.cssText += c
			}) : this
		},

		//
		// ClassList API methods
		// ---------------------
		//

		// __addClass__( `value [value] || function` )  
		// set the className(s) on each item in a collection 
		addClass: function (a) {
			return i(a, 'add', this)
		},

		// __removeClass__( `value [value] || function` )  
		// remove the className(s) on each item in a collection
		removeClass: function (a) {
			return i(a, 'remove', this)
		},
	
		// __toggleClass__( `value [value] || function` )  
		// toggle the className(s) on each item in a collection
		toggleClass: function (a) {
			return i(a, 'toggle', this)
		},
	
		// __hasClass__( `value [value]` )  
		// returns boolean `true` if every item in the collection has the className(s)
		hasClass: function (a, b, i, d) {
			i = this.length
			b = "" + a === a
			var foo = function (a) { return d[c].contains(a) }
			while (b && i--) {
				d = this[i];
				b = a.split(s).every(foo)
			}
			return b
		}
	})

}(huka, 'classList', /\s+/, /^f/))


//		dom.js 1.0
//		huka plugin that provides dom manipulation functionality
//

;(function($, c, p, i){
	
	//
	// Setup
	// -------
	//
	
	
	// Convert a string to a dom node and prepend or append to each item in a collection
	i = function(a, b) {
		a = ("" + a === a) ? (p.innerHTML = a) && p.firstChild : a
		return this.each(function(e){
			e.parentNode.insertBefore(a.cloneNode(true), b ? e.nextSibling : e)
		})
	}
	
	// Reusable element used to convert a string to an html fragment
	p = document.createElement('div')
	
	
	// 
	// Extend huka.prototype
	// ---------------------
	//
	return $.fn.extend({


	//
	// Attribute methods
	// -----------------
	//
	

	// __setAttribute__( `property, value || { key: 'value' }` )
	//
	// sets an attribute on each item in the collection
	setAttribute: function(a,b) {
		return this.each(function(e){
			if("" + a === a) e.setAttribute(a, b || '')
			else for(var c in a) e.setAttribute(c, a[c] || '')
		})
	},
	
	// __removeAttribute__( `property || { key: 'any value' }` )
	//
	// removes an attribute from each item in a collection
	removeAttribute: function(a,b) {
		return this.each(function(e){
			if("" + a === a) e.removeAttribute(a)
			else for(var c in a) e.removeAttribute(c)  
		})
	},
	
	// __getAttribute__( `property` )
	//
	// get the value of an attribute from the first item in a collection
	getAttribute: function(a) {
		return this[0].getAttribute(a);
	},
	
	//
	// Visibility methods
	// ------------------
	//
	
	// __hide__()
	//
	// hide all items in a collection
	hide: function() {
		return this.setAttribute(c)
	},
	
	// __show__()
	//
	// show all items in a collection
	show: function(){
		return this.removeAttribute(c)
	},
	
	// __toggle__()
	//
	// toggles the hidden attribute for each item in a collection
	toggle: function(a) {
		
		return this.each(function(e){
			a = (e.getAttribute(c) !== null ? 'remove' : 'set') + 'Attribute'
			e[a](c) 
		})
	},
	
	//
	// Dom manipulation methods
	// ------------------------
	//

	// __prepend__( `html string || dom node` )
	//
	// prepends an html string or dom node to each item in a collection
	prepend: function(a) {
		return i.call(this, a)
	},
	
	// __append__( `html string || dom node` )
	//
	// appends an html string or dom node to each item in a collection
	append: function(a) {
		return i.call(this, a, true)
	},
	
	// __remove__()
	//
	// removes dom nodes matching the selector
	remove: function() {
		return this.each(function(e){
			e.parentNode.removeChild(e)
		})
	},

	// __replacewith__( `html string || dom node` )
	//
	// replaces an existing dom node with an html string or dom node
	replacewith: function(c){
		return this.prepend(c).remove()
    }




})

}(huka, 'hidden'))
//		eventst.js 1.0
//		huka plugin that provides event handling functionality
//

;(function($, c) {

	//
	// Extend huka.prototype
	// ---------------------
	//
	return $.fn.extend({

		// __on__( `event, function` )
		//
		// Register a callback function for an event on each item in a collection
		on: function(a, b) {
			return this.each(function(e) {
				e['add' + c](a, b)
			})
		},


		// __off__( `event, function` )
		//
		// Deregister a callback function for an event on each item in a collection
		off: function(a, b) {
			return this.each(function(e){
				e['remove' + c](a, b)
			})
		},

		// __trigger__( `event` )
		//
		// Fire an event on each item in a collection
		trigger: function(a, b) {
			return this.each(function(e){
				b  = new Event(a, this, true)
				e.dispatchEvent(b)
			})
		}
	})

})(huka, 'EventListener')
;(function ($) {
	 var serialize = function() {
		var form = this[0];
		if (!form || form.nodeName !== "FORM") {
			return;
		}
		var i, j, q = [];
		for (i = form.elements.length - 1; i >= 0; i = i - 1) {
			if (form.elements[i].name === "") {
				continue;
			}
			switch (form.elements[i].nodeName) {
			case 'INPUT':
				switch (form.elements[i].type) {
				case 'text':
				case 'hidden':
				case 'password':
				case 'button':
				case 'reset':
				case 'submit':
					q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
					break;
				case 'checkbox':
				case 'radio':
					if (form.elements[i].checked) {
						q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
					}						
					break;
				case 'file':
					break;
				}
				break;			 
			case 'TEXTAREA':
				q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
				break;
			case 'SELECT':
				switch (form.elements[i].type) {
				case 'select-one':
					q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
					break;
				case 'select-multiple':
					for (j = form.elements[i].options.length - 1; j >= 0; j = j - 1) {
						if (form.elements[i].options[j].selected) {
							q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].options[j].value));
						}
					}
					break;
				}
				break;
			case 'BUTTON':
				switch (form.elements[i].type) {
				case 'reset':
				case 'submit':
				case 'button':
					q.push(form.elements[i].name + "=" + encodeURIComponent(form.elements[i].value));
					break;
				}
				break;
			}
		}
		return q.join("&");
	}

	var submit = function(a, b) {
		b = this
		b.on('submit', function(e) {
			e.preventDefault()
			b.ajax(b.getAttribute('method'), b.getAttribute('action'), a, b.serialize())
		})
	}
	
	
	// Extend prototype
	return $.fn.extend({
		serialize: serialize,
		submit: submit
	})

})(huka)